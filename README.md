# Gitlab Integration plugin for Confluence

Using this plugin you can connect to a gitlab instance and display the release notes of a project in a page.

At first you need to add the gitlab's url and an access token in the configuration
![](src/main/resources/images/01-configure-plugin.png)

then in the page you want to display the releasesadd the `Gitlab Release Notes` macro
![](src/main/resources/images/02-select-macro.png)

and set the project id
![](src/main/resources/images/03-configure-macro.png)

You have successfully created an Atlassian Plugin!

Here are the SDK commands you'll use immediately:

* atlas-run -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-help -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK

