package it.gr.aalexandrakis.plugins.confluence;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;
import gr.aalexandrakis.plugins.confluence.api.GitlabIntegrationPlugin;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AtlassianPluginsTestRunner.class)
public class MyComponentWiredTest {
    private final ApplicationProperties applicationProperties;
    private final GitlabIntegrationPlugin gitlabReleaseNotesPlugin;

    public MyComponentWiredTest(ApplicationProperties applicationProperties, GitlabIntegrationPlugin gitlabReleaseNotesPlugin) {
        this.applicationProperties = applicationProperties;
        this.gitlabReleaseNotesPlugin = gitlabReleaseNotesPlugin;
    }

    static {
        System.setProperty("baseurl", "http://localhost:1111");
    }

    @Test
    public void testMyName() {
        System.out.println("app properties display name : " + applicationProperties.getDisplayName());
        System.out.println("gitlab properties name : " + gitlabReleaseNotesPlugin.getName());
        assertEquals("names do not match!", applicationProperties.getDisplayName(), gitlabReleaseNotesPlugin.getName());
    }
}