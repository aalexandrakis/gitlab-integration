package gr.aalexandrakis.plugins.confluence;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import gr.aalexandrakis.plugins.confluence.model.Config;
import org.apache.commons.httpclient.HttpClientError;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.IOException;

@Path("/")
@Scanned
public class ConfigResource {
    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
    @ComponentImport
    private final TransactionTemplate transactionTemplate;

    private static final Logger log = LoggerFactory.getLogger(ConfigResource.class);

    @Inject
    public ConfigResource(UserManager userManager, PluginSettingsFactory pluginSettingsFactory,
                          TransactionTemplate transactionTemplate) {
        this.userManager = userManager;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@Context HttpServletRequest request) throws IOException {
        log.info("Get Config");
        UserProfile userProfile = userManager.getRemoteUser(request);
        if (userProfile == null || !userManager.isSystemAdmin(userProfile.getUserKey())) {
            return Response.status(Status.UNAUTHORIZED).build();
        }

        Config config = (Config) transactionTemplate.execute(new TransactionCallback() {
            public Config doInTransaction() {
                PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
                Config config = new Config();
                config.setUrl((String) settings.get(Config.class.getName() + ".url"));
                //TODO decrypt token here
                config.setToken((String) settings.get(Config.class.getName() + ".token"));

                return config;
            }
        });
        if (config.getUrl() != null && config.getToken() != null) {
            checkGitlabConnection(config);
        }
        return Response.ok(config).build();
    }

    @GET
    @Path("internal")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInternal(@Context HttpServletRequest request) throws IOException {
        log.info("Get Config internal");
        UserProfile userProfile = userManager.getRemoteUser(request);
        if (userProfile == null) {
            return Response.status(Status.UNAUTHORIZED).build();
        }

        Config config = (Config) transactionTemplate.execute(new TransactionCallback() {
            public Config doInTransaction() {
                PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
                Config config = new Config();
                config.setUrl((String) settings.get(Config.class.getName() + ".url"));
                //TODO decrypt token here
                config.setToken((String) settings.get(Config.class.getName() + ".token"));
                return config;
            }
        });
        return Response.ok(config).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(final Config config, @Context HttpServletRequest request) throws IOException {
        log.info("Put Config");

        UserProfile userProfile = userManager.getRemoteUser(request);
        if (userProfile == null || !userManager.isSystemAdmin(userProfile.getUserKey())) {
            return Response.status(Status.UNAUTHORIZED).build();
        }

        if (config.getToken() != null || config.getUrl() != null) {
            checkGitlabConnection(config);
        }

        transactionTemplate.execute(new TransactionCallback() {
            public Object doInTransaction() {
                PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
                pluginSettings.put(Config.class.getName() + ".url", config.getUrl());
                //TODO Encrypt token here
                pluginSettings.put(Config.class.getName() + ".token", config.getToken());
                return null;
            }
        });
        return Response.noContent().build();
    }

    private void checkGitlabConnection(Config config) throws IOException {
        log.info("Check gitlab connection");
        HttpGet httpGet = new HttpGet(config.getUrl() + "/api/v4/user/status");
        httpGet.addHeader("PRIVATE-TOKEN", config.getToken());
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(httpGet)) {
            // Get HttpResponse Status

            log.info(response.getStatusLine().toString());
            HttpEntity entity = response.getEntity();
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new HttpClientError("Failed to connect");
            }
            if (entity != null) {
                String result = EntityUtils.toString(entity);
                log.info(result);
            }
        } catch (IOException e) {
            log.error("Unable to connect to Gitlab Instance in " + config.getUrl(), e);
            throw e;
        }
    }

}

