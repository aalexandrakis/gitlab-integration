
package gr.aalexandrakis.plugins.confluence.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GitlabReleaseNote {

    @JsonProperty(value = "assets")
    private Assets mAssets;
    @JsonProperty(value = "author")
    private Author mAuthor;
    @JsonProperty(value = "commit")
    private Commit mCommit;
    @JsonProperty(value = "commit_path")
    private String mCommitPath;
    @JsonProperty(value = "created_at")
    private String mCreatedAt;
    @JsonProperty(value = "description")
    private String mDescription;
    @JsonProperty(value = "description_html")
    private String mDescriptionHtml;
    @JsonProperty(value = "evidences")
    private List<Evidence> mEvidences;
    @JsonProperty(value = "name")
    private String mName;
    @JsonProperty(value = "released_at")
    private String mReleasedAt;
    @JsonProperty(value = "tag_name")
    private String mTagName;
    @JsonProperty(value = "tag_path")
    private String mTagPath;
    @JsonProperty(value = "upcoming_release")
    private Boolean mUpcomingRelease;
    @JsonProperty(value = "_links")
    private _links m_links;

    public Assets getAssets() {
        return mAssets;
    }

    public void setAssets(Assets assets) {
        mAssets = assets;
    }

    public Author getAuthor() {
        return mAuthor;
    }

    public void setAuthor(Author author) {
        mAuthor = author;
    }

    public Commit getCommit() {
        return mCommit;
    }

    public void setCommit(Commit commit) {
        mCommit = commit;
    }

    public String getCommitPath() {
        return mCommitPath;
    }

    public void setCommitPath(String commitPath) {
        mCommitPath = commitPath;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDescriptionHtml() {
        return mDescriptionHtml;
    }

    public void setDescriptionHtml(String descriptionHtml) {
        mDescriptionHtml = descriptionHtml;
    }

    public List<Evidence> getEvidences() {
        return mEvidences;
    }

    public void setEvidences(List<Evidence> evidences) {
        mEvidences = evidences;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getReleasedAt() {
        return mReleasedAt;
    }

    public void setReleasedAt(String releasedAt) {
        mReleasedAt = releasedAt;
    }

    public String getTagName() {
        return mTagName;
    }

    public void setTagName(String tagName) {
        mTagName = tagName;
    }

    public String getTagPath() {
        return mTagPath;
    }

    public void setTagPath(String tagPath) {
        mTagPath = tagPath;
    }

    public Boolean getUpcomingRelease() {
        return mUpcomingRelease;
    }

    public void setUpcomingRelease(Boolean upcomingRelease) {
        mUpcomingRelease = upcomingRelease;
    }

    public _links get_links() {
        return m_links;
    }

    public void set_links(_links _links) {
        m_links = _links;
    }

    public static class _links {

        @JsonProperty(value = "edit_url")
        private String mEditUrl;
        @JsonProperty(value = "self")
        private String mSelf;

        public String getEditUrl() {
            return mEditUrl;
        }

        public void setEditUrl(String editUrl) {
            mEditUrl = editUrl;
        }

        public String getSelf() {
            return mSelf;
        }

        public void setSelf(String self) {
            mSelf = self;
        }

    }

    public static class Assets {

        @JsonProperty(value = "count")
        private Long mCount;
        @JsonProperty(value = "links")
        private List<Object> mLinks;
        @JsonProperty(value = "sources")
        private List<Source> mSources;

        public Long getCount() {
            return mCount;
        }

        public void setCount(Long count) {
            mCount = count;
        }

        public List<Object> getLinks() {
            return mLinks;
        }

        public void setLinks(List<Object> links) {
            mLinks = links;
        }

        public List<Source> getSources() {
            return mSources;
        }

        public void setSources(List<Source> sources) {
            mSources = sources;
        }

    }

    public static class Author {

        @JsonProperty(value = "avatar_url")
        private String mAvatarUrl;
        @JsonProperty(value = "id")
        private Long mId;
        @JsonProperty(value = "name")
        private String mName;
        @JsonProperty(value = "state")
        private String mState;
        @JsonProperty(value = "username")
        private String mUsername;
        @JsonProperty(value = "web_url")
        private String mWebUrl;

        public String getAvatarUrl() {
            return mAvatarUrl;
        }

        public void setAvatarUrl(String avatarUrl) {
            mAvatarUrl = avatarUrl;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getState() {
            return mState;
        }

        public void setState(String state) {
            mState = state;
        }

        public String getUsername() {
            return mUsername;
        }

        public void setUsername(String username) {
            mUsername = username;
        }

        public String getWebUrl() {
            return mWebUrl;
        }

        public void setWebUrl(String webUrl) {
            mWebUrl = webUrl;
        }

    }

    public static class Commit {

        @JsonProperty(value = "author_email")
        private String mAuthorEmail;
        @JsonProperty(value = "author_name")
        private String mAuthorName;
        @JsonProperty(value = "authored_date")
        private String mAuthoredDate;
        @JsonProperty(value = "committed_date")
        private String mCommittedDate;
        @JsonProperty(value = "committer_email")
        private String mCommitterEmail;
        @JsonProperty(value = "committer_name")
        private String mCommitterName;
        @JsonProperty(value = "created_at")
        private String mCreatedAt;
        @JsonProperty(value = "id")
        private String mId;
        @JsonProperty(value = "message")
        private String mMessage;
        @JsonProperty(value = "parent_ids")
        private List<String> mParentIds;
        @JsonProperty(value = "short_id")
        private String mShortId;
        @JsonProperty(value = "title")
        private String mTitle;
        @JsonProperty(value = "web_url")
        private String mWebUrl;

        public String getAuthorEmail() {
            return mAuthorEmail;
        }

        public void setAuthorEmail(String authorEmail) {
            mAuthorEmail = authorEmail;
        }

        public String getAuthorName() {
            return mAuthorName;
        }

        public void setAuthorName(String authorName) {
            mAuthorName = authorName;
        }

        public String getAuthoredDate() {
            return mAuthoredDate;
        }

        public void setAuthoredDate(String authoredDate) {
            mAuthoredDate = authoredDate;
        }

        public String getCommittedDate() {
            return mCommittedDate;
        }

        public void setCommittedDate(String committedDate) {
            mCommittedDate = committedDate;
        }

        public String getCommitterEmail() {
            return mCommitterEmail;
        }

        public void setCommitterEmail(String committerEmail) {
            mCommitterEmail = committerEmail;
        }

        public String getCommitterName() {
            return mCommitterName;
        }

        public void setCommitterName(String committerName) {
            mCommitterName = committerName;
        }

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getId() {
            return mId;
        }

        public void setId(String id) {
            mId = id;
        }

        public String getMessage() {
            return mMessage;
        }

        public void setMessage(String message) {
            mMessage = message;
        }

        public List<String> getParentIds() {
            return mParentIds;
        }

        public void setParentIds(List<String> parentIds) {
            mParentIds = parentIds;
        }

        public String getShortId() {
            return mShortId;
        }

        public void setShortId(String shortId) {
            mShortId = shortId;
        }

        public String getTitle() {
            return mTitle;
        }

        public void setTitle(String title) {
            mTitle = title;
        }

        public String getWebUrl() {
            return mWebUrl;
        }

        public void setWebUrl(String webUrl) {
            mWebUrl = webUrl;
        }

    }

    public static class Evidence {

        @JsonProperty(value = "collected_at")
        private String mCollectedAt;
        @JsonProperty(value = "filepath")
        private String mFilepath;
        @JsonProperty(value = "sha")
        private String mSha;

        public String getCollectedAt() {
            return mCollectedAt;
        }

        public void setCollectedAt(String collectedAt) {
            mCollectedAt = collectedAt;
        }

        public String getFilepath() {
            return mFilepath;
        }

        public void setFilepath(String filepath) {
            mFilepath = filepath;
        }

        public String getSha() {
            return mSha;
        }

        public void setSha(String sha) {
            mSha = sha;
        }

    }

    public static class Source {

        @JsonProperty(value = "format")
        private String mFormat;
        @JsonProperty(value = "url")
        private String mUrl;

        public String getFormat() {
            return mFormat;
        }

        public void setFormat(String format) {
            mFormat = format;
        }

        public String getUrl() {
            return mUrl;
        }

        public void setUrl(String url) {
            mUrl = url;
        }

    }
}
