package gr.aalexandrakis.plugins.confluence.impl;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import gr.aalexandrakis.plugins.confluence.model.Config;
import gr.aalexandrakis.plugins.confluence.model.GitlabReleaseNote;
import org.apache.commons.httpclient.HttpClientError;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Scanned
public class GitlabReleaseNotesMacroImpl implements Macro {

    private final XhtmlContent xhtmlUtils;

    private static final Logger log = LoggerFactory.getLogger(GitlabReleaseNotesMacroImpl.class);

    @Autowired
    public GitlabReleaseNotesMacroImpl(@ComponentImport XhtmlContent xhtmlUtils) {
        this.xhtmlUtils = xhtmlUtils;
    }

    @Override
    public String execute(Map<String, String> map, String s, ConversionContext conversionContext) throws MacroExecutionException {
        String projectId = map.get("projectId");
        String result = "";
        try {
            Config config = new Config();
            config.setUrl(map.get("gitlabUrl"));
            config.setToken(map.get("gitlabToken"));
            HttpGet httpGet = new HttpGet(config.getUrl() + "/api/v4/projects/" + projectId + "/releases");
            httpGet.addHeader("PRIVATE-TOKEN", config.getToken());
            CloseableHttpClient httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(httpGet);
            // Get HttpResponse Status
            log.info(response.getStatusLine().toString());
            HttpEntity entity = response.getEntity();
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new HttpClientError("Failed to connect");
            }
            if (entity != null) {
                String responseString = EntityUtils.toString(entity);
                System.out.println("Gitlab Response " + responseString);
                List<GitlabReleaseNote> gitlabReleaseNotes = new ObjectMapper().readValue(responseString, new TypeReference<List<GitlabReleaseNote>>() {
                });
                for (GitlabReleaseNote gitlabReleaseNote : gitlabReleaseNotes) {
                    result += "<h3>" + gitlabReleaseNote.getName() + "</h3>";
                    result += "<strong>Released On:</strong> " + gitlabReleaseNote.getReleasedAt().substring(0, 10) + " ";
                    result += "<strong>Released By:</strong> " +
                            "<img src=\"" + gitlabReleaseNote.getAuthor().getAvatarUrl() + "\" style=\"width: 20px;\"></img>" +
                            " " + gitlabReleaseNote.getAuthor().getName();
                    result += "<br/>";
                    result += gitlabReleaseNote.getDescriptionHtml();
                }
            }
        } catch (ClientProtocolException clientProtocolException) {
            clientProtocolException.printStackTrace();
            throw new MacroExecutionException("Unable to retrieve release notes from Gitlab fot project id " + projectId);
        } catch (IOException ioException) {
            ioException.printStackTrace();
            throw new MacroExecutionException("Unable to retrieve release notes from Gitlab fot project id " + projectId);
        }
        return result;
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
