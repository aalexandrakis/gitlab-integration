package gr.aalexandrakis.plugins.confluence.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import gr.aalexandrakis.plugins.confluence.api.GitlabIntegrationPlugin;

import javax.inject.Inject;
import javax.inject.Named;

@ExportAsService({GitlabIntegrationPlugin.class})
@Named("GitlabIntegration")
public class GitlabIntegrationPluginImpl implements GitlabIntegrationPlugin {
    @ComponentImport
    private final ApplicationProperties applicationProperties;

    @Inject
    public GitlabIntegrationPluginImpl(final ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public String getName()
    {
        if(null != applicationProperties)
        {
            return applicationProperties.getDisplayName();
        }
        
        return "gitlabReleaseNotes";
    }
}