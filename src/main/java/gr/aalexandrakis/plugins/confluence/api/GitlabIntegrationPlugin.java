package gr.aalexandrakis.plugins.confluence.api;

public interface GitlabIntegrationPlugin {
    String getName();
}