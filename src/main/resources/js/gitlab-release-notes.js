AJS.MacroBrowser.setMacroJsOverride("gitlab-release-notes", {
    beforeParamsSet: function (params) {
        var request = new XMLHttpRequest();
        request.open('GET', AJS.contextPath() + "/rest/gitlab-integration/1.0/internal", false);
        request.send(null);
        if (request.status === 200) {
            var gitlabIntegrationConfig = JSON.parse(request.responseText)
            params.gitlabUrl = gitlabIntegrationConfig.url;
            params.gitlabToken = gitlabIntegrationConfig.token;
        }
        return params
    },
    fields: {
        string: {
            "gitlabUrl": function (param, options) {
                var parameterField = AJS.MacroBrowser.ParameterFields["_hidden"](param, options);
                console.log(param)
                parameterField.setValue(param.gitlabUrl);
                return parameterField
            },
            "gitlabToken": function (param, options) {
                var parameterField = AJS.MacroBrowser.ParameterFields["_hidden"](param, options);
                console.log(param)
                parameterField.setValue(param.gitlabToken);
                return parameterField
            }
        },
    }
});

