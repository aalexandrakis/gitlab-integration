(function ($) { // this closure helps us keep our variables to ourselves.
// This pattern is known as an "iife" - immediately invoked function expression

    // form the URL
    var url = AJS.contextPath() + "/rest/gitlab-integration/1.0/";

    // wait for the DOM (i.e., document "skeleton") to load. This likely isn't necessary for the current case,
    // but may be helpful for AJAX that provides secondary content.
    $(document).ready(function() {
        // request the config information from the server
        $.ajax({
            url: url,
            dataType: "json"
        }).done(function(config) { // when the configuration is returned...
            AJS.$("#admin").submit(function(e) {
                e.preventDefault();
                updateConfig();
            });
            $("#url").val(config.url);
            $("#token").val(config.token);
        });
    });

})(AJS.$ || jQuery);

function updateConfig() {
    AJS.$.ajax({
        url: AJS.contextPath() + "/rest/gitlab-integration/1.0/",
        type: "PUT",
        contentType: "application/json",
        data: '{ "url": "' + AJS.$("#url").attr("value") + '", "token": "' + AJS.$("#token").attr("value") + '" }',
        processData: false,
        success: function () {
            AJS.flag({
                type: 'success',
                body: 'Gitlab configuration saved.',
                title: "Gitlab Integration Configuration"
            });
        },
        error: function () {
            AJS.flag({
                type: 'error',
                body: 'Fail to save gitlab configuration, check url or token and try again',
                title: "Gitlab Integration Configuration"
            });
        },
    });
}